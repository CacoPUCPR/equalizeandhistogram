import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Marcos on 02/04/17.
 */
public class Histogram {


    //histogram function
    private int[] makeHistogram(BufferedImage img){

        int[] histogramArray = new int[256]; // create an array to be the histogram

        for (int xPosition = 0; xPosition < img.getWidth(); xPosition++) {

            for (int yPosition = 0; yPosition < img.getHeight(); yPosition++) {

                Color color = new Color(img.getRGB(xPosition, yPosition)); //pick pixel color

                int r = color.getRed(); //as we working with gray scale, we will just pick one color channel

                histogramArray[r] += 1; //store the pixel in the gray tone relative to it. Example, if the red in color is 25 it will store one
            }                           //more value in the slot 25
        }

        return histogramArray;
    }


    private int[] makeAccumulatedHistogram(int[] histogramArray){

            int[] accumulatedHistogramArray = histogramArray; //make a copy of the real histogram

            for (int i = 1; i < accumulatedHistogramArray.length; i++) {

                accumulatedHistogramArray[i] += accumulatedHistogramArray[i-1]; //increases the backward pixels to the next index
            }

            return accumulatedHistogramArray;
    }


    private int initialTone(int[] histogram){

        for (int i = 0; i < histogram.length; i++) {

            if(histogram[i] != 0) //confers and return the first tone with pixels in it
                return histogram[i];

        }

        return 0;
    }


    private int[] makeMap(int[] histogram, int totalPixels){

        int[] map = new int[256];
        int totalTones = 256;
        int[] accumulated = makeAccumulatedHistogram(histogram);

        float initialTone = initialTone(histogram); // it has to be float to dont get a div in the equation down here

        for (int i = 0; i < histogram.length; i++) {

            //making new tone -> (round(ha(v) - hmin / pixels - hmin)) * (tons -1)
            map[i] = Math.round(((accumulated[i] - initialTone) / (totalPixels - initialTone)) * (totalTones - 1));


        }

        return map;
    }


    private BufferedImage equalize(BufferedImage img){

        BufferedImage out = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_RGB);

        int[] histogram = makeHistogram(img); // make the histogram with passed image
        int[] map = makeMap(histogram, img.getWidth() * img.getHeight()); // make the map with histogram

        for (int x = 0; x < img.getWidth(); x++) {

            for (int y = 0; y < img.getHeight(); y++) {

                Color color = new Color(img.getRGB(x, y));

                int newTone = map[color.getRed()]; // pick color of pixel and put it as index in the array map

                Color newColor = new Color(newTone, newTone, newTone);
                out.setRGB(x,y, newColor.getRGB());
            }
        }
        return out;
    }


    public void run() throws IOException {

        BufferedImage car = ImageIO.read(new File("src/car.png"));
        BufferedImage cars = ImageIO.read(new File("src/cars.jpg"));
        BufferedImage crowd = ImageIO.read(new File("src/crowd.png"));
        BufferedImage mountain = ImageIO.read(new File("src/montanha.jpg"));
        BufferedImage university = ImageIO.read(new File("src/university.png"));
        BufferedImage lizard = ImageIO.read(new File("src/lizard.png"));

        BufferedImage[]imageArray = {car, cars, crowd, mountain, university, lizard};

        for (int i = 0; i<imageArray.length;i++){
            BufferedImage newImage = equalize(imageArray[i]);
            ImageIO.write(newImage, "png", new File("image-" + i + ".png"));
        }




    }



    static public void main (String[] args) throws IOException {
        new Histogram().run();
    }
}
